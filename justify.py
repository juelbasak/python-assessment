# Complete

import sys

def justify(string, width):
    mylist = string.split()
    s = 0
    newlist = []
    for i in mylist:
        s += len(i)
    
    n_space = (width - s) // (len(mylist) - 1)
    r = (width - s) % (len(mylist) - 1)

    for i in range(len(mylist)):
        newlist.append(mylist[i])
        if i != len(mylist)-1:
            if r == 0:
                newlist.append(n_space)
            else:
                newlist.append(n_space+1)
                r -= 1

    return newlist

with open(sys.argv[1], 'r+') as f:
    while True:
        contents = f.readline()
        if not contents:
            break
        mylist = justify(contents.strip(), int(sys.argv[2].strip()))
        newlist = []
        for i in range(len(mylist)):
            if i % 2 == 0:
                newlist.append(mylist[i])
            else:
                for j in range(mylist[i]):
                    newlist.append(' ')

        print(''.join(newlist))




